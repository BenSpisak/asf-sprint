Rails.application.routes.draw do

  devise_for :users, controllers: {:registrations => "registrations", :confirmations => 'confirmations'}

  match '/users',   to: 'users#index',   via: 'get'
  match '/users/:id',     to: 'users#show',       via: 'get'

  resources :users

  get 'forgot_password/forgot'

  get 'pages/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'

  # makes /login go to the login page
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
  end

  # makes /signup go to the signup page
  devise_scope :user do
    get 'signup', to: 'devise/registrations#new'
  end

  # makes /signup go to the signup page
  devise_scope :user do
    get 'edit', to: 'devise/registrations#edit'
  end

  get '/search' => 'pages#search', :as => 'search_page'

  post 'users/search' => 'users#search', as: 'search_user'

  resources :registration_steps
  resources :event_infos

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
end
