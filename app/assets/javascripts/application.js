// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require jquery3
//= require popper
//= require bootstrap

function showImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#your_preview_id')
          .attr('src', e.target.result)
          .width(150)
          .height(200);
      };

      reader.readAsDataURL(input.files[0]);
    }
  };
function toggleColors() {

}

function changeBodyBg(color, color2, color3, color4, color5, color6){
    var elements = document.querySelectorAll(".turn-color");

    if(document.getElementById("homepage-bodytext").style.color === color2){
      document.getElementById("navbar").style.backgroundColor = color5;
      document.getElementById("homepage-bodytext").style.color = color4;
      document.body.style.backgroundColor = color;
      for (var i = 0; i < elements.length; i++) {
        elements[i].style.color = color4;
      }
    }else{
      document.getElementById("navbar").style.backgroundColor = color6;
      document.getElementById("homepage-bodytext").style.color = color2;
      document.body.style.backgroundColor = color3;
      for (var i = 0; i < elements.length; i++) {
        elements[i].style.color = color2;
      }

    }
};

$scope.cardViewClick =  function() {
  $timeout(function() {
    $scope.cardView = true;
  }, 500);
    $scope.listView = false;
};
$scope.listViewClick =  function() {
    $scope.cardView = false;
  $timeout(function() {
    $scope.listView = true;
  }, 500);
};
