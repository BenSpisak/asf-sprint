class UsersController < ApplicationController
  before_action :authenticate_user!
  
  def index
    if params[:name]
      @user = User.where('name LIKE ?', "%#{params[:name]}%")
    else
      @user = User.all
    end
  end

  def edit
    @user = current_user
  end

  def show
    @user = User.find(params[:id])
  end

  def search
    @q = "%#{params[:query]}%"
    @user = User.where("firstname LIKE ? or lastname LIKE ? or year LIKE ? or location LIKE ?", @q, @q, @q, @q)
    render 'index'
  end

  def user_params
    params.require(:user).permit(:firstname, :lastname, :email, :bio, :year, :location)
  end

end
