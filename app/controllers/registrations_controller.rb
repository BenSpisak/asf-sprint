class RegistrationsController < Devise::RegistrationsController

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    super
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    super
  end

  def after_sign_up_path_for(resource)
    new_confirmation_path
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    confirmation_path(resource)
  end

    private

    def sign_up_params
      params.require(:user).permit( :firstname,
                                    :lastname,
                                    :email,
                                    :bio,
                                    :year,
                                    :location,
                                    :password,
                                    :password_confirmation)
    end

    def account_update_params
      params.require(:user).permit( :firstname,
                                    :lastname,
                                    :email,
                                    :bio,
                                    :year,
                                    :location,
                                    :password,
                                    :password_confirmation,
                                    :current_password)
    end

  end
