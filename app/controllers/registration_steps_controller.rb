class RegistrationStepsController < ApplicationController
  include Wicked::Wizard
  steps :information, :biography, :availability

  def new
    @user = User.new
  end

  def create
      @user = User.new(params[:sign_up])
      if @user.save
        session[:user_id] = @user.id
        redirect_to user_steps_path
      else
        redirect_to new_user_registration_path
      end
    end

  def show
    @user = current_user
    render_wizard
  end

  def update
    @user = current_user
    @user.update_attributes(user_params)
    render_wizard @user
  end

  private
    def user_params
      params.require(:user).permit(:firstname, :lastname, :email, :password, :bio, :year, :location, :business, :degrees, :share, :mentor, :image, :contact, :attending)
    end
end
