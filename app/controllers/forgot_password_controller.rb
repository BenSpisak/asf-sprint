class ForgotPasswordController < ApplicationController
  def forgot
  end

  def send_email
  	if params[:search].blank?
    		redirect_to(root_path, alert: 'Empty field!') and return
    else
    	@parameter = params[:search].downcase
    		#@results = User.where('lower(email) LIKE :search', search: @parameter)
    		@results = User.exists('email LIKE :search', search: @parameter)
  	end
  end

end
