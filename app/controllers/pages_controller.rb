class PagesController < ApplicationController
  def index
    @user1970 = User.where('year < 1980 AND year > 1960')
    @user1980 = User.where('year < 1990 AND year > 1979')
    @user1990 = User.where('year < 2000 AND year > 1989')
    @user2000 = User.where('year < 2010 AND year > 1999')
    @user2010 = User.where('year < 2020 AND year > 2009')
  end

 	def search
  		if params[:search].blank?
    		redirect_to(root_path, alert: 'Empty field!') and return
  		else
    		@parameter = params[:search].downcase+'%'
    		@results = User.where('lower(firstname) LIKE :search OR lower(lastname) LIKE :search OR year LIKE :search OR lower(business) LIKE :search or lower(email) LIKE :search OR lower(location) LIKE :search', search: @parameter).order(:year)
		end

	end
end
